import React from 'react';
import styled from 'styled-components';
import Section from './Section';

function Home() {
  return (
    <Container>
      <Section 
        title="Model S"
        desc="Order Online for Touchless Delivery"
        leftBtn="Custom Order"
        rightBtn="Existing Inventory"
        imgURl="model-s.jpg"
      />
      <Section 
        title="Model 3"
        desc="Order Online for Touchless Delivery"
        leftBtn="Custom Order"
        rightBtn="Existing Inventory"
        imgURl="model-3.jpg"
      />
      {/* <Section 
        title="Model X"
        desc="Order Online for Touchless Delivery"
        leftBtn="Custom Order"
        rightBtn="Existing Inventory"
        imgURl="model-x.jpg"
      /> */}
      <Section 
        title="Model Y"
        desc="Order Online for Touchless Delivery"
        leftBtn="Custom Order"
        rightBtn="Existing Inventory"
        imgURl="model-y.jpg"
      />
      <Section 
        title="Lowest Cost Solar Panels in America"
        desc="Money-back guarante"
        leftBtn="Order Now"
        rightBtn="Lean More"
        imgURl="solar-panel.jpg"
      />
      <Section 
        title="Solar for New Roofs"
        desc="Solar Roof Cost Less Than a New Root Plus Solar Panel"
        leftBtn="Order Now"
        rightBtn="Lean More"
        imgURl="solar-roof.jpg"
      />
      <Section 
        title="Accessories"
        desc= ""
        leftBtn="Shop Now"
        rightBtn=""
        imgURl="accessories.jpg"
      />
    </Container>
  )
}

export default Home;

const Container=styled.div`
  height: 100vh;
`